La Balsamine
==========

Saison 2012-2013
----

Second year of full communication for this brussels theater : programme booklet with poster cover, various posters, website, signage and flyers. Plus some surprises. And a performance.
We use Ume Libre fonts, Scribus, Inkscape, Gimp, Graphviz, Pmwiki, Fontforge and many other F/LOSS tools. Graphic design : mainly Ludi, Steph and Pierre.

http://www.balsamine.be

Partner : Speculoos - http://speculoos.com


